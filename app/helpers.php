<?php

use Illuminate\Support\Facades\Log;


if (! function_exists('show_route')) {
    /**
     * Turn a valid xml string to array
     *
     * @return string
     */
    function xml_to_array($xmlString)
    {
        if (xml_is_valid($xmlString)) {
            return json_decode(json_encode((new SimpleXMLElement($xmlString, LIBXML_NOCDATA))), true);
        }

        return false;
    }
}

if (! function_exists('xml_is_valid')) {
    /**
     * Validates if a XML is right
     *
     * @return boolean
     */
    function xml_is_valid($xml, $version = '1.0', $encoding = 'utf-8')
    {
        if (trim($xml) == '') {
            return false;
        }

        libxml_use_internal_errors(true);

        $doc = new DOMDocument($version, $encoding);
        $doc->loadXML($xml);

        $errors = libxml_get_errors();

        libxml_clear_errors();

        return empty($errors);
    }
}

