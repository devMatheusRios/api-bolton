<?php

namespace App\Transformers;

use App\Models\Nota;
use League\Fractal\TransformerAbstract;

class NotaTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Nota $nota)
    {
        return [
            'valorNota' => $nota->valor
        ];
    }
}
