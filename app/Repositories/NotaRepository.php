<?php

namespace App\Repositories;


use App\Models\Nota;

class NotaRepository extends BaseRepository
{

    protected $modelClass = Nota::class;

    /**
     * Method to get a invoice by key
     *
     * @param $key
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Pagination\AbstractPaginator
     */
    public function getNotaByKey($key)
    {
        $query = $this->newQuery();
        $query->where('chave', $key);

        return $this->doQuery($query, 1, false);
    }
}