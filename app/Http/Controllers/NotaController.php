<?php

namespace App\Http\Controllers;

use App\Repositories\NotaRepository;
use App\Transformers\NotaTransformer;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class NotaController extends Controller
{

    private $notaRepository;

    public function __construct()
    {
        $this->notaRepository = new NotaRepository();
    }

    /**
     * Method to get a invoid value
     * @param $chave
     * @return mixed
     */
    public function show($chave)
    {
        $nota = $this->notaRepository->getNotaByKey($chave);

        return fractal($nota, new NotaTransformer())->toArray();
    }

    public function getInvoicesFromAPI()
    {
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'x-api-id' => 'p9TFaeVaOhFlpCKADbEqZBff014RwVZp',
            'x-api-key' => 'cxEtSjOKCZAf0KZFznmkO6gARCHcAdS1'
        ];

        try {

            $res = $client->request('GET', 'https://api.arquivei.com.br/v1/nfe/received', [
                'headers' => $headers
            ]);

            if($res->getStatusCode() === 200) {
                $contents = $res->getBody()->getContents();

                $json = json_decode($contents);

                if(count($json->data)) {
                    foreach($json->data as $item) {
                        $array = xml_to_array(base64_decode($item->xml));

                        $infoNFe = $array['NFe']['infNFe'];

                        $this->notaRepository->create(['chave' => $item->access_key, 'valor' => $infoNFe['total']['ICMSTot']['vNF']]);
                    }
                }
            }

            return response()->json(['Notas retornadas com sucesso!'], 200);

        } catch (ClientException $ex) {
            return response()->json([['errors' => $ex->getMessage()]], $ex->getResponse()->getStatusCode());
        }

    }

}
