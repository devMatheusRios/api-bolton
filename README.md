# Api Bolton

Api para consulta de notas fiscais importadas da API da Arquivei

## Iniciando

A seguir é informado o mínimo necessário para rodar o projeto.

### Pré requisitos

Será necessário ter no seu ambiente de desenvolvimento o composer, PHP na versão 7.3 e ainda algumas de suas extensões (como todo projeto Laravel).
Para saber mais, recomendo ler a seção "Server Requirements" na documentação do Laravel.
https://laravel.com/docs/5.8/installation

No projeto, existe um arquivo chamado .env.example, você vai precisar renomeá-lo para .env
e configurar as conexões de banco de dados MySQL, informar usuário, senha e o nome de um banco existente.


### Instalação

Após clonar o projeto, instale suas dependências

```
composer install
```

Rode 

```
php artisan generate:key
```

e logo a seguir rode as migrations

```
php artisan migrate
```

Depois é só rodar o projeto com o seguinte comando

```
php artisan serve
```

### Endpoints

Para importar as notas fiscais da API da Arquivei, é necessário rodar o seguinte endpoint:

GET - http://localhost/api/notas-from-api

Para consultar o valor de uma nota fiscal específica é só fazer a seguinte requisição:

GET - http://localhost/api/notas/{chave}




